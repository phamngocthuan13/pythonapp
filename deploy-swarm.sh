echo 'deploy app to server PROD: => '
rm -rf $HOME/pythonapp
git clone https://gitlab.com/phamngocthuan13/pythonapp.git
cd $HOME/pythonapp
docker rmi registry.gitlab.com/phamngocthuan13/pythonapp:latest
docker stack deploy --compose-file docker-compose-prod.yml --with-registry-auth stackpython
#
echo '==> deploy success on PROD server'
